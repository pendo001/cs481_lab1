import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lab1/locale/app_localization.dart';

void main() {
  runApp(MyApp());
}

enum Majors { computer_science, math, history }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  AppLocalizationDelegate _localeOverrideDelegate =
      AppLocalizationDelegate(Locale('en', 'US'));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          _localeOverrideDelegate
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('es', 'MX')
        ],
        title: 'Lab 1',
        home: screenMain(),
    );
  }
} //My App

class getSimpleDialog extends StatefulWidget {
  @override
  _getSimpleDialogState createState() => _getSimpleDialogState();
}

class _getSimpleDialogState extends State<getSimpleDialog> {
  String Major;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
        padding: EdgeInsets.all(20),
        alignment: Alignment.center,
        child: RaisedButton(
            child: Text(AppLocalization.of(context).buttonLabel2),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title:
                          Text(AppLocalization.of(context).buttonLabel2_title),
                      children: <Widget>[
                        SimpleDialogOption(
                          onPressed: () {
                            Navigator.pop(
                                context,
                                Major = AppLocalization.of(context)
                                    .buttonLabel2_CS);
                          },
                          child:
                              Text(AppLocalization.of(context).buttonLabel2_CS),
                        ),
                        SimpleDialogOption(
                          onPressed: () {
                            Navigator.pop(
                                context,
                                Major = AppLocalization.of(context)
                                    .buttonLabel2_MTH);
                          },
                          child: Text(
                              AppLocalization.of(context).buttonLabel2_MTH),
                        ),
                        SimpleDialogOption(
                          onPressed: () {
                            Navigator.pop(
                                context,
                                Major = AppLocalization.of(context)
                                    .buttonLabel2_HST);
                          },
                          child: Text(
                              AppLocalization.of(context).buttonLabel2_HST),
                        ),
                      ],
                    );
                  }).then((_) => setState(() {}));
            }),
      ),
      Container(
          padding: EdgeInsets.all(20),
          child: Text.rich(TextSpan(children: [
            TextSpan(
                text: AppLocalization.of(context).buttonLabel2_output,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            TextSpan(text: Major),
          ])))
    ]);
  }
}

class getAlertName extends StatefulWidget {
  @override
  _getAlertNameState createState() => _getAlertNameState();
}

class _getAlertNameState extends State<getAlertName> {
  TextEditingController nameController = TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
          padding: EdgeInsets.all(20),
          alignment: Alignment.center,
          child: RaisedButton(
              child: Text(AppLocalization.of(context).buttonLabel1),
              onPressed: () {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) {
                      return AlertDialog(
                          title: Text(
                              AppLocalization.of(context).buttonLabel1_title),
                          content: new Row(children: [
                            new Expanded(
                              child: new TextField(controller: nameController),
                            )
                          ]));
                    }).then((_) => setState(() {}));
              })),
      Container(
          padding: EdgeInsets.all(20),
          child: Text.rich(TextSpan(children: [
            TextSpan(
                text: AppLocalization.of(context).buttonLabel1_output,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            TextSpan(text: nameController.text),
          ])))
    ]);
  }
}

class buttonLanguage extends StatefulWidget {
  @override
  _buttonLanguageState createState() => _buttonLanguageState();
}

class _buttonLanguageState extends State<buttonLanguage> {
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('Language'),
        Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                child: Text('English'),
                onPressed: () {
                  setState(() {
                    AppLocalization.load(Locale('en', 'US'));
                  });
                },
              ),
              RaisedButton(
                child: Text('Spanish'),
                onPressed: () {
                  setState(() {
                    AppLocalization.load(Locale('es', 'MX'));
                  });
                },
              ),
            ]),
      ],
    );
  }
}

class screenMain extends StatefulWidget{
  @override
  _screenMainState createState() => _screenMainState();
}

class _screenMainState extends State<screenMain>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title: Text(AppLocalization.of(context).textScaffold),
        ),
        body: ListView(children: [
          Container(
              child: Image.asset(
                'images/School-PNG-Pic.png',
                width: 250,
                height: 350,
              )),
          getAlertName(),
          getSimpleDialog(),
          buttonLanguage()
        ]));
  }
}
