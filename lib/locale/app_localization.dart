import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:lab1/l10n/messages_all.dart';

class AppLocalization {
  static Future<AppLocalization> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalization();
    });
  }

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  // list of locales
  String get textScaffold {
    return Intl.message(
      'Student Information',
      name: 'textScaffold',
      desc: 'Title Text',
    );
  }
  String get buttonLabel1 {
    return Intl.message(
      'Enter Your Name',
      name: 'buttonLabel1',
      desc: 'Prompt to enter name',
    );
  }
  String get buttonLabel1_title {
    return Intl.message(
      'Enter Name',
      name: 'buttonLabel1_title',
      desc: 'Dialogue Label',
    );
  }
  String get buttonLabel1_output {
    return Intl.message(
      'Name: ',
      name: 'buttonLabel1_output',
      desc: 'Label to entered name',
    );
  }
  String get buttonLabel2 {
    return Intl.message(
      'Pick A Major',
      name: 'buttonLabel2',
      desc: 'Prompt to choose major',
    );
  }
  String get buttonLabel2_title {
    return Intl.message(
      'Select A Major',
      name: 'buttonLabel2_title',
      desc: 'Dialogue label',
    );
  }
  String get buttonLabel2_CS {
    return Intl.message(
      'Computer Science',
      name: 'buttonLabel2_CS',
    );
  }
  String get buttonLabel2_MTH {
    return Intl.message(
      'Math',
      name: 'buttonLabel2_MTH',
    );
  }
  String get buttonLabel2_HST {
    return Intl.message(
      'History',
      name: 'buttonLabel2_HST',
    );
  }
  String get buttonLabel2_output {
    return Intl.message(
      'Major: ',
      name: 'buttonLabel2_output',
      desc: 'Label to chosen major',
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalization> {
  final Locale overriddenLocale;

  const AppLocalizationDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) => AppLocalization.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) => false;
}
