// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "buttonLabel1" : MessageLookupByLibrary.simpleMessage("Introduzca su nombre"),
    "buttonLabel1_output" : MessageLookupByLibrary.simpleMessage("Nombre: "),
    "buttonLabel1_title" : MessageLookupByLibrary.simpleMessage("Ingrese su nombre"),
    "buttonLabel2" : MessageLookupByLibrary.simpleMessage("Elija una especialidad"),
    "buttonLabel2_CS" : MessageLookupByLibrary.simpleMessage("Ciencias de la Computación"),
    "buttonLabel2_HST" : MessageLookupByLibrary.simpleMessage("Historia"),
    "buttonLabel2_MTH" : MessageLookupByLibrary.simpleMessage("Matemáticas"),
    "buttonLabel2_output" : MessageLookupByLibrary.simpleMessage("Especialidad: "),
    "buttonLabel2_title" : MessageLookupByLibrary.simpleMessage("Seleccione una especialidad"),
    "textScaffold" : MessageLookupByLibrary.simpleMessage("Información del estudiante")
  };
}
